function sortCharacter(string) {

    /* ==== Sort character with array.sort() === */

    //return string.split('').sort().join('')

    
    let stringArray = []

    for (let i = 0; i < string.length; i++) {
        stringArray.push(string[i])
    }

    for (let i = 1; i < stringArray.length; i++) {
        for (let j = 0; j < i; j++) {
            if(stringArray[i] > stringArray[j]) continue
            
            let temp = stringArray[i]
            stringArray[i] = stringArray[j] 
            stringArray[j] = temp
        }
    }

    return stringArray.join('')
}
